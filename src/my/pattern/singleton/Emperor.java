package my.pattern.singleton;

public class Emperor {
	// private static Emperor emperor = null;
	private static Emperor emperor = new Emperor();
	private Emperor() {
		// 不允许产生其他emperor
	}

	// public static Emperor getInstance() {
	// if (emperor == null) {
	// emperor = new Emperor();
	// }
	// return emperor;
	// }
	/**
	 * 通用单例模式
	 * 
	 * @return
	 */
	public static synchronized Emperor getInstance() {
		return emperor;
	}
	public void emperorInfo() {
		System.out.println("皇帝？？");
	}
}
