package my.pattern.singleton;

/**
 * 
 * 单例模式：Singleton：保证一个类仅有一个实例，并提供一个访问它的全局访问点。
 *  {@link Emperor}构造方法，不能new新的单例
 */
public class Minister {
	public static void main(String[] args) {
		Emperor em = Emperor.getInstance();
		em.emperorInfo();
		Emperor em2 = Emperor.getInstance();
		em2.emperorInfo();
		Emperor em3 = Emperor.getInstance();
		em3.emperorInfo();
	}
}
