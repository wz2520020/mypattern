package my.pattern.strategy;

public interface IStrategy {
	public void operate();// 都是一个可执行算法
}
