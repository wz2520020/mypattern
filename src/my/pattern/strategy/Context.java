package my.pattern.strategy;

public class Context {
	private IStrategy is;

	public Context(IStrategy is) {// 构造函数，将需要调用的方法装进来
		this.is = is;
	}
	// 方法调用
	public void operate() {
		this.is.operate();
	}
}
