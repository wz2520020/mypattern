package my.pattern.strategy;

/**
 * 
 * 策略模式： 定义一系列的算法,把它们一个个封装起来, 并且使它们可相互替换。本模式使得算法的变化可独立于使用它的客户。
 *  Context为模式关键，构造方法，用同一个方法，分别调用不同的实现类
 */
public class Use {

	public static void main(String[] args) {
		Context context;
		// 第一个方法
		IStrategy is = new Method1();
		context = new Context(is);
		context.operate();
		// 第二个方法
		context = new Context(new Method2());
		context.operate();
		// 第3个方法
		context = new Context(new Method3());
		context.operate();
	}
}
