package my.pattern.proxy;

/**
 * 
 * 代理模式:为其他对象提供一个代理以控制对这个对象的访问。 Woman,为代理模式关键。通过构造方法，代理不同的实现方式
 */
public class Man {
	public static void main(String[] args) {
		Woman wm = new Woman();
		wm.doSomeThing();
		wm.doSomeThing2();
		wm = new Woman(new Boy());
		wm.doSomeThing();
		wm.doSomeThing2();
	}
}
