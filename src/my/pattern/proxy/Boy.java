package my.pattern.proxy;

public class Boy implements IWomen {

	@Override
	public void doSomeThing() {
		System.out.println("say:boy1");
	}

	@Override
	public void doSomeThing2() {
		System.out.println("say:boy2");

	}

}
