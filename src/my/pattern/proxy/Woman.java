package my.pattern.proxy;

public class Woman implements IWomen {
	private IWomen iw;

	public Woman() {
		this.iw = new Girl();
	}

	public Woman(IWomen iw) {
		this.iw = iw;
	}
	@Override
	public void doSomeThing() {
		this.iw.doSomeThing();
	}

	@Override
	public void doSomeThing2() {
		iw.doSomeThing2();
	}

}
